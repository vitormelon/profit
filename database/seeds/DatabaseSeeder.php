<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);

        factory(App\produto::class, 50)->create();
        factory(App\fornecedor::class, 10)->create();
        factory(App\Clientes::class, 10)->create();
    }
}