<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\produto::class, function (Faker $faker) {
    return [
        'nome' => $faker->name,
        'codigo' => $faker->unique()->randomNumber(),
        'descricao' => $faker->text($maxNbChars = 50),
        'precoVenda' => $faker->randomNumber(3),
        'precoCusto' => $faker->randomNumber(3),
        'ativo' => 1
    ];
});