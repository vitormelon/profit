<?php

use Faker\Generator as Faker;

$factory->define(App\Clientes::class, function (Faker $faker) {
    return [
        'nome' => $faker->name,
        'endereco' => $faker->streetName(),
        'instagram' => $faker->word(),
        'facebook' => $faker->word(),
        'email' => $faker->safeEmail(),
        'celular' => $faker->phoneNumber(),
        'telefone' => $faker->phoneNumber(),
        'observacoes' => $faker->text(100)
    ];
});