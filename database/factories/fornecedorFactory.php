<?php

use Faker\Generator as Faker;

$factory->define(App\fornecedor::class, function (Faker $faker) {
    return [
        'nome' => $faker->name(),
        'endereco' => $faker->streetName(),
        'cpfcnpj' => $faker->word(),
        'email' => $faker->safeEmail(),
        'telefone1' => $faker->phoneNumber(),
        'telefone2' => $faker->phoneNumber(),
        'telefone3' => $faker->phoneNumber(),
        'observacoes' => $faker->text(100),
        'ativo' => true
    ];
});
