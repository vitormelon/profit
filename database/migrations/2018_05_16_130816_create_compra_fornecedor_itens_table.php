<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompraFornecedorItensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compra_fornecedor_itens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quantidade')->nullable();
            $table->float('valorCusto')->nullable();
            $table->unsignedInteger('compra_fornecedor_id')->nullable();
            $table->foreign('compra_fornecedor_id')->references('id')->on('compra_fornecedores');
            $table->unsignedInteger('produto_id')->nullable();
            $table->foreign('produto_id')->references('id')->on('produtos');
            $table->unsignedInteger('tamanho_produto_id')->nullable();
            $table->foreign('tamanho_produto_id')->references('id')->on('tamanhos_produto');
            $table->unsignedInteger('estoque_id')->nullable();
            $table->foreign('estoque_id')->references('id')->on('estoques');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compra_fornecedor_itens');
    }
}
