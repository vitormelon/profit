<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTamanhosProdutoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tamanhos_produto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome')->unique();
            $table->boolean('ativo')->default(false);
            $table->timestamps();
        });

        DB::table('tamanhos_produto')->insert(
            [
                ['nome' => 'P','ativo' => true],
                ['nome' => 'M','ativo' => true],
                ['nome' => 'G','ativo' => true],
                ['nome' => 'GG','ativo' => true],
                ['nome' => '36','ativo' => true],
                ['nome' => '38','ativo' => true],
                ['nome' => '40','ativo' => true],
                ['nome' => '42','ativo' => true],
            ]
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tamanhos_produto');
    }
}
