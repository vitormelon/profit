<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstoquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estoques', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quantidade');
            $table->float('valorCusto')->nullable();
            $table->unsignedInteger('tamanho_produto_id')->nullable();
            $table->foreign('tamanho_produto_id')->references('id')->on('tamanhos_produto');
            $table->unsignedInteger('fornecedor_id')->nullable();
            $table->foreign('fornecedor_id')->references('id')->on('fornecedores');
            $table->unsignedInteger('unidade_id')->nullable();
            $table->foreign('unidade_id')->references('id')->on('unidades');
            $table->unsignedInteger('produto_id');
            $table->foreign('produto_id')->references('id')->on('produtos');
            $table->unique(['produto_id', 'unidade_id', 'fornecedor_id', 'tamanho_produto_id'], 'estoque_unique');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estoques');
    }
}
