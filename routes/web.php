<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('', 'HomeController@index')->name('home');
Route::resource('clientes', 'ClientesController')->middleware('auth');
Route::resource('produto', 'ProdutoController')->middleware('auth');
Route::resource('tamanhoProduto', 'TamanhoProdutoController')->middleware('auth');
Route::resource('fornecedor', 'FornecedorController')->middleware('auth');
Route::resource('unidade', 'UnidadeController')->middleware('auth');
Route::resource('estoque', 'EstoqueController')->middleware('auth');
Route::resource('compra', 'CompraController')->middleware('auth');
Route::resource('compraRest', 'CompraRestController')->middleware('auth');
Route::get('/produtoApi/nextid', 'ProdutoController@getNextID')->name('produtoNextID')->middleware('auth');
Route::post('/compraRest/{id}', 'CompraRestController@update')->middleware('auth');

