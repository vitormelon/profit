<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('nome', 'Nome')}}
            {{Form::text('nome', null, array('placeholder' => '','class' => 'form-control'))}}

        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('ativo', 'Ativo')}}
            {{Form::hidden('ativo',0)}}
            {{ Form::checkbox('ativo', 1, !isset($unidade['ativo']) || $unidade['ativo'] ? true : false)}}
        </div>
    </div>
</div>
<button type="submit" class="btn btn-primary">Gravar</button>