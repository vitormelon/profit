<div class="row">

    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {{Form::label('codigo', 'Codigo')}}
                {{ Form::text('codigo', null, array('placeholder' => '','class' => 'form-control')) }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {{Form::label('nome', 'Nome')}}
                {{ Form::text('nome', null, array('placeholder' => '','class' => 'form-control')) }}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {{Form::label('precoVenda', 'Preco Venda')}}
                {{ Form::text('precoVenda', null, array('placeholder' => '','class' => 'form-control')) }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {{Form::label('precoCusto', 'Preco Custo')}}
                {{ Form::number('precoCusto', null, array('placeholder' => '','class' => 'form-control')) }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {{Form::label('descricao', 'Descricao')}}
                {{ Form::text('descricao', null, array('placeholder' => '','class' => 'form-control')) }}
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <br>
                    @if(isset($produto->imagem))
                        <img class="produto-imagem img-responsive center-block" src="{{url($produto->path . "thumb" .$produto->imagem)}}"><br>
                    @endif
                {{Form::file('imagem', array('placeholder' => '','class' => 'center-margin'))}}

            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">
            @if(isset($produto->estoque))
            <table class="table table-hover" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th scope="col">Tamanho</th>
                    <th scope="col">Quantidade</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($produto->estoque as $estoque)
                        <tr>
                            <td scope="row" >{{$estoque->tamanhoProduto->nome}}</td>
                            <td scope="row" >{{$estoque->quantidade}}</td>
                            <td scope="row">
                                <a href="{{ URL::to('estoque/' . $estoque->id . '/edit') }}" class="btn btn-primary btn-xs">
                                    <i class="fa fa-pencil"></i> Ver
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<button type="submit" class="btn btn-primary">Gravar</button>