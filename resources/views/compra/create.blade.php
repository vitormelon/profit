@extends('layouts.blank')

@section('main_container')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Incluir Compra</h2>
                    <ul class="navbar-right panel_toolbox">
                        <li>
                            <a href="{{ URL::previous() }}" class="btn btn-primary btn-xs">
                                <i class="fa fa-arrow-left"></i> Voltar
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('compra.form')
@endsection