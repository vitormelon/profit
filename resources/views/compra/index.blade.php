@extends('layouts.blank')
 
@section('main_container')

        <div class="col-md-12 col-sm-12 col-xs-12">
            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}2</div>
            @endif
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Compras</h2>
                    <ul class="navbar-right panel_toolbox">
                        <li>
                            <a href="{{Route('compra.create')}}" class="btn btn-primary btn-xs">
                                <i class="fa fa-plus"></i> Inserir
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Data</th>
                            <th scope="col">Descrição</th>
                            <th scope="col">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($compras as $compra)
                            <tr>
                                <td scope="row">{{$compra->id}}</td>
                                <td scope="row">{{$compra->data}}</td>
                                <td scope="row">{{$compra->descricao}}</td>
                                <td scope="row">
                                    @component('components.actions', ['entidade' => 'compra', 'id' => $compra->id])@endcomponent
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

@endsection