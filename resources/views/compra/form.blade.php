<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="x_panel title">
            <div class="x_title">
                <h2>Dados Compra</h2>
                <ul class="navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                @if(isset($compra->id))
                {!! Form::model($compra, ['method' => 'PATCH','route' => ['compra.update', $compra->id], 'files' => true]) !!}
                @else
                {!! Form::open(array('route' => 'compra.store','method'=>'POST')) !!}
                @endif
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        {{Form::label('unidade', 'Unidade')}}
                        {{Form::select('unidade_id', $unidades, null, ['class' => 'form-control', 'select2' => 'true'])}}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        {{Form::label('descricao', 'Descrição')}}
                        {{Form::text('descricao', null, array('placeholder' => '','class' => 'form-control'))}}

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        {{Form::label('data', 'Data')}}
                        {{Form::date('data', isset($compra->data) ? $compra->data : null , array('placeholder' => '','class' => 'form-control'))}}

                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        {{Form::label('observacoes', 'Observações')}}
                        {{Form::textarea('observacoes', null, array('placeholder' => '','class' => 'form-control'))}}

                    </div>
                </div>
                <button type="submit" class="btn btn-primary" style="float:right">Gravar</button>
                {{Form::close()}}
            </div>
        </div>
    </div>
    @if(isset($compra->id))
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="x_panel">
            <div class="x_title">
                <h2>Fornecedores</h2>
                <ul class="navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-xs-12 col-sm-12 col-md-12" id="fornecedores">
                </div>
                <script id="fornecedoresTemplate" type="x-tmpl-mustache">
                    <div class="form-group">
                        <div style=" padding-right: 10px; float:left; width: calc(100% - 30px);">
                            <select class="form-control" name="fornecedor_id" select2Tag="true">
                                <option selected="selected" value="">Selecione...</option>
                                @{{#fornecedores}}
                                <option value="@{{ id }}">@{{ id }} - @{{ nome }}</option>
                                @{{/fornecedores}}
                            </select>
                        </div>
                        <div style="display: inline">
                            <span href="#" class="btn btn-primary btn-xs" onclick="addFornecedor()">
                                <i class="fa fa-plus"></i>
                            </span>
                        </div>

                    </div><br>
                    <table class="table table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th scope="col">Fornecedor</th>
                            <th scope="col" class="text-center">Qtd. Produtos</th>
                            <th scope="col" class="text-center"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @{{#compraFornecedores}}
                        <tr>
                            <td scope="row" >@{{fornecedor.id}} - @{{fornecedor.nome}}</td>
                            <td scope="row" class="text-center">@{{ produtos.length }}</td>
                            <td scope="row" class="text-center" >
                                <span class="btn btn-primary btn-xs" onclick="selectfornecedor(@{{ id }})">
                                    <i class="fa fa-pencil"></i>
                                </span>
                                <span href="" class="btn btn-danger btn-xs" onclick="removeFornecedor(@{{ id }})">
                                    <i class="fa fa-trash-o"></i>
                                </span>
                            </td>
                        </tr>
                        @{{/compraFornecedores}}
                        </tbody>
                    </table>
                </script>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div id="produtos"></div>
        <script id="produtosTemplate" type="x-tmpl-mustache">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Produtos - @{{ selectedFornecedor.fornecedor.nome }}</h2>
                    <ul class="navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <div style=" padding-right: 10px; float:left; width: calc(100% - 30px);">
                                <select class="form-control" name="produto_id" select2="true">
                                    <option selected="selected" value="">Novo...</option>
                                    @{{#produtos}}
                                    <option value="@{{ id }}">@{{ id }} - @{{ nome }}</option>
                                     @{{/produtos}}
                                </select>
                            </div>
                            <div style="display: inline">
                                <a class="btn btn-primary btn-xs" onclick="formProduto(@{{ id }})">
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>

                        </div><br>
                        <table class="table table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th scope="col">Código</th>
                                    <th scope="col">Produto</th>
                                    <th scope="col" class="text-center">Quantidade</th>
                                    <th scope="col" class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                            @{{#selectedFornecedor.produtos}}
                                <tr>
                                    <td scope="row" >@{{ id }}</td>
                                    <td scope="row" >@{{ nome }}</td>
                                    <td scope="row" class="text-center">@{{pivot.quantidade}}</td>
                                    <td scope="row" class="text-center" >
                                        <a href="" class="btn btn-primary btn-xs">
                                            <i class="fa fa-pencil"></i> Ver
                                        </a>
                                    </td>
                                </tr>
                            @{{/selectedFornecedor.produtos}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </script>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6" id="produto">
    </div>
        <script id="produtoTemplate" type="x-tmpl-mustache">.
            <div class="x_panel">
                <div class="x_title">
                    <h2>Produto</h2>
                    <ul class="navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form id="formProduto" onsubmit="addProduto(this);return false;" enctype='multipart/form-data' >
                        {{ csrf_field() }}
                        <input name="produtoId" type="hidden" value="@{{ produto.id }}">
                        <input name="addproduto" type="hidden" value="1">
                        <input name="unidade_id" type="hidden" value="{{$compra->unidade->id}}">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                {{Form::label('fornecedor', 'Fornecedor')}}
                                <input name="compra_fornecedor_id" type="hidden" value="@{{ selectedFornecedor.id }}">
                                <select class="form-control" name="select" disabled="true">
                                    <option value="@{{ selectedFornecedor.fornecedor.id }}" selected="selected">@{{ selectedFornecedor.fornecedor.id }} - @{{ selectedFornecedor.fornecedor.nome }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                            <br>
                            <input placeholder="" class="center-margin" name="imagem" type="file">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                            <label for="codigo">Codigo</label>
                            <input placeholder="" class="form-control" id="produtoCodigo" value="@{{ produto.codigo }}" name="codigo" type="text">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="nome">Nome</label>
                                <input placeholder="" class="form-control" name="nome" value="@{{ produto.nome }}" type="text" id="nome">
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="precoVenda">Preco Venda</label>
                                <input placeholder="" class="form-control" name="precoVenda" value="@{{ produto.precoVenda }}" type="text" id="precoVenda">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="precoCusto">Preco Custo</label>
                                <input placeholder="" class="form-control" name="precoCusto" value="@{{ produto.precoCusto }}"  type="number" id="precoCusto">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="descricao">Descricao</label>
                                <input placeholder="" class="form-control" name="descricao" value="@{{ produto.Descricao }}" type="text" id="descricao">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                {{Form::label('tamanho', 'Tamanho')}}
                                {{Form::select('tamanho_produto_id', $tamanhosProduto, null, ['class' => 'form-control' , 'placeholder' => 'Selecione...', 'select2' => 'true'])}}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                {{Form::label('quantidade', 'Quantidade')}}
                                {{Form::text('quantidade', null, array('placeholder' => '','class' => 'form-control'))}}
                            </div>
                        </div>
                        <button class="btn btn-primary" style="float:right" >Gravar</button>
                    </form>
                </div>
            </div>
        </script>

        <div id="erro"></div>
</div>

<script>
    var token = "{{Session::token()}}";
    var urlCompraUpdate = '{{Route("compraRest.update", $compra->id)}}';
    var urlCompraEdit = '{{Route("compraRest.edit", $compra->id)}}';
    var urlProdutoNextId = '{{ route('produtoNextID') }}';

    window.onload = function() {
        refreshDadosCompra();
    };

    function formProduto(){
        var data = mergeSelectedFornecedorDadosCompra();
        var template = $('#produtoTemplate').html();
        console.log(data);
        Mustache.parse(template);   // optional, speeds up future uses
        var rendered = Mustache.render(template, data);
        $('#produto').html(rendered);
        getNextId();
        updateSelect2();
        scrollTo("#produto");
    }

    function getNextId(){
        $.ajax({
            method: 'GET',
            url: urlProdutoNextId,
            success: function (data) {
                $('#produtoCodigo').val(data.id);
            },
            error: AjaxFailed
        })
    }

    function addProduto(form){

        var data = new FormData(form);
        console.log(data);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': token
            }
        });

        $.ajax({
            method: 'POST',
            url: urlCompraUpdate,
            data: data,
            success: function (data) {
                console.log(data);
            },
            error: AjaxFailed,
            cache: false,
            processData: false,
            dataType:'json',
            contentType: false
        })

        return false;
    }

    function selectfornecedor(id){
        $("#produto").html(null);
        storeSelectedFornecedor(id);
        loadProdutos();
        updateSelect2();
        scrollTo("#produtos");
    }

    function loadProdutos(){
        var data = mergeSelectedFornecedorDadosCompra();
        console.log(data);
        if(data.selectedFornecedor === undefined){
            $('#produtos').html(null);
            return;
        }
        var template = $('#produtosTemplate').html();
        Mustache.parse(template);   // optional, speeds up future uses
        var rendered = Mustache.render(template, data);
        $('#produtos').html(rendered);
        updateSelect2();
    }

    function addFornecedor(){
        var fornecedor = $("select[name=fornecedor_id]").val();
        if(fornecedor == ''){
            return;
        }

        if(!$.isNumeric(fornecedor)){
            if(!confirm("Deseja criar o novo fornecedor: " + fornecedor + ' ?')){
                return;
            }
        }

        $.ajax({
            method: 'PUT',
            url: urlCompraUpdate,
            data: { addfornecedor: fornecedor, _token: token },
            success: function (data) {
                refreshDadosCompra();
            },
            error: AjaxFailed
        })
    }

    function removeFornecedor(id){
        if(!confirm(`Tem certeza que deseja remover o fornecedor de id: ${id} ?`)){
            return;
        }

        $.ajax({
            method: 'PUT',
            url: urlCompraUpdate,
            data: { removefornecedor: id, _token: token },
            success: function (data) {
                unsetSelectedFornecedor();
                refreshDadosCompra();
            },
            error: AjaxFailed
        })
    }

    function refreshDadosCompra(){
        $.ajax({
            method: 'GET',
            url: urlCompraEdit,
            success: function (data) {
                console.log(data);
                storeDadosCompra(data);
                loadFornecedores();
                loadProdutos();
            },
            error: AjaxFailed
        })
    }

    function loadFornecedores() {
        var data = retriveDadosCompra();
        var template = $('#fornecedoresTemplate').html();
        Mustache.parse(template);   // optional, speeds up future uses
        var rendered = Mustache.render(template, data);
        $('#fornecedores').html(rendered);
        updateSelect2();
    }

    function AjaxFailed(result) {
        console.log(result.responseText);
        $("#erro").html(result.responseText);
        alert('Erro');
    }
    function updateSelect2(){
        $('select[select2=true]').select2();

        $('select[select2Tag=true]').select2({
            tags:true
        });
    }

    function storeDadosCompra(data){
        window.dadosCompra = data;
    }
    function retriveDadosCompra(){
        return window.dadosCompra;
    }
    function storeSelectedFornecedor(id){
        dadosCompra =retriveDadosCompra();
        compraFornecedor = dadosCompra.compraFornecedores.find(x => x.id==id);
        window.selectedFornecedor = {"selectedFornecedor": compraFornecedor};
    }
    function retriveSelectedFornecedor(){
        return window.selectedFornecedor;
    }
    function mergeSelectedFornecedorDadosCompra(){
        return $.extend(retriveDadosCompra(),retriveSelectedFornecedor());
    }
    function unsetSelectedFornecedor(){
        delete window.selectedFornecedor;
    }
    function scrollTo(element) {
        $('html, body').animate({
            scrollTop: $(element).offset().top
        }, 500);
    }
    function getFormData(id){
        var form = $(id);
        var unindexed_array = form.serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });

        return indexed_array;
    }

</script>
@endif
