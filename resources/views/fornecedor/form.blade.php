<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('nome', 'Nome')}}
            {{Form::text('nome', null, array('placeholder' => '','class' => 'form-control'))}}

        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('email', 'E-mail')}}
            {{ Form::text('email', null, array('placeholder' => '','class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('endereco', 'Endereço')}}
            {{ Form::text('endereco', null, array('placeholder' => '','class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('cpfcnpj', 'CPF/CNPJ')}}
            {{ Form::text('cpfcnpj', null, array('placeholder' => '','class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('telefone1', 'Telefone 1')}}
            {{ Form::text('telefone1', null, array('placeholder' => '','class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('telefone2', 'Telefone 2')}}
            {{ Form::text('telefone2', null, array('placeholder' => '','class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('telefone3', 'Telefone 3')}}
            {{ Form::text('telefone3', null, array('placeholder' => '','class' => 'form-control')) }}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('observacoes', 'Observações')}}
            {{ Form::textarea('observacoes', null, array('placeholder' => '','class' => 'form-control')) }}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('ativo', 'Ativo')}}
            {{Form::hidden('ativo',0)}}
            {{ Form::checkbox('ativo', 1, !isset($fornecedor['ativo']) || $fornecedor['ativo'] ? true : false)}}
        </div>
    </div>
</div>
<button type="submit" class="btn btn-primary">Gravar</button>