<a href="{{ URL::to($entidade.'/' . $id . '/edit') }}" class="btn btn-primary btn-xs">
    <i class="fa fa-pencil"></i>
</a>
{{Form::open(['method'  => 'DELETE', 'route' => [$entidade.'.destroy', $id]])}}
{{Form::button('<i class="fa fa-trash-o"></i>', array('type' => 'submit', 'onclick' => 'return buttonConfirmation(this);','class' => 'btn btn-danger btn-xs', 'data-confirm'=>"Tem certeza que deseja excluir?"))}}
{{Form::close() }}