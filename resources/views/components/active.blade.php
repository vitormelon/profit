@if($active)
    {{Form::button('<i class="fa fa-check"></i> Ativo', array('class' => 'btn btn-success btn-xs cursorDefault'))}}
@else
    {{Form::button('<i class="fa fa-times"></i> Inativo', array('class' => 'btn btn-danger btn-xs cursorDefault'))}}
@endif