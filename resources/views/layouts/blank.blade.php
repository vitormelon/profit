<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Profit</title>
        <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
        <!-- Bootstrap -->
        <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="{{ asset("css/gentelella.custom.css") }}" rel="stylesheet">
        {{--DataTables--}}
        <link href="{{ asset("css/dataTables.bootstrap.min.css") }}" rel="stylesheet">
        <link href="{{ asset("css/responsive.bootstrap.min.css") }}" rel="stylesheet">
        {{--Custom--}}
        <link href="{{ asset("css/custom.css") }}" rel="stylesheet">
        {{--Select2--}}
        <link href="{{ asset("css/select2.css") }}" rel="stylesheet">

        @stack('stylesheets')

    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">

                @include('includes/sidebar')

                @include('includes/topbar')
                <!-- page content -->
                <div class="right_col" role="main">
                    @yield('main_container')
                </div>

                @include('includes/footer')

            </div>
        </div>

        <!-- jQuery -->
        <script src="{{ asset("js/jquery.min.js") }}"></script>
        <!-- Bootstrap -->
        <script src="{{ asset("js/bootstrap.min.js") }}"></script>
        <!-- Custom Theme Scripts -->
        <script src="{{ asset("js/gentelella.custom.js") }}"></script>


        <script src="{{ asset("js/jquery.dataTables.min.js") }}"></script>
        <script src="{{ asset("js/dataTables.bootstrap.min.js") }}"></script>

        <script src="{{ asset("js/dataTables.responsive.min.js") }}"></script>
        <script src="{{ asset("js/responsive.bootstrap.js") }}"></script>
        <script src="{{ asset("js/custom.js") }}"></script>
        <script src="{{ asset("js/select2.full.js") }}"></script>
        <script src="{{ asset("js/mustache.js") }}"></script>
        @stack('scripts')

    </body>
</html>