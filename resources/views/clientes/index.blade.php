@extends('layouts.blank')
 
@section('main_container')

        <div class="col-md-12 col-sm-12 col-xs-12">
            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Clientes</h2>
                    <ul class="navbar-right panel_toolbox">
                        <li>
                            <a href="{{Route('clientes.create')}}" class="btn btn-primary btn-xs">
                                <i class="fa fa-plus"></i> Inserir
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Celular</th>
                            <th scope="col">facebook</th>
                            <th scope="col">Instagram</th>
                            <th scope="col" >Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($clientes as $cliente)
                            <tr>
                                <th scope="row">{{$cliente->id}}</th>
                                <td>{{$cliente->nome}}</td>
                                <td>{{$cliente->celular}}</td>
                                <td>{{$cliente->facebook}}</td>
                                <td>{{$cliente->instagram}}</td>
                                <td>
                                    @component('components.actions', ['entidade' => 'clientes', 'id' => $cliente->id])
                                    @endcomponent
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>





@endsection