<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="form-group">
            {{Form::label('nome', 'Nome')}}
            {{ Form::text('nome', null, array('placeholder' => '','class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="form-group">
            {{Form::label('endereco', 'Endereço')}}
            {{ Form::text('endereco', null, array('placeholder' => '','class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="form-group">
            {{Form::label('instagram', 'Instagram')}}
            {{ Form::text('instagram', null, array('placeholder' => '','class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="form-group">
            {{Form::label('facebook', 'Facebook')}}
            {{ Form::text('facebook', null, array('placeholder' => '','class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="form-group">
            {{Form::label('email', 'Email')}}
            {{ Form::text('email', null, array('placeholder' => '','class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="form-group">
            {{Form::label('celular', 'Celular')}}
            {{ Form::text('celular', null, array('placeholder' => '','class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="form-group">
            {{Form::label('telefone', 'Telefone')}}
            {{ Form::text('telefone', null, array('placeholder' => '','class' => 'form-control')) }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('observacoes', 'Observacoes')}}
            {{ Form::textarea('observacoes', null, array('placeholder' => '','class' => 'form-control')) }}
        </div>
    </div>
</div>
<button type="submit" class="btn btn-primary">Gravar</button>
