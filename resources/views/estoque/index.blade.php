@extends('layouts.blank')
 
@section('main_container')

        <div class="col-md-12 col-sm-12 col-xs-12">
            @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Unidades</h2>
                    <ul class="navbar-right panel_toolbox">
                        <li>
                            <a href="{{Route('estoque.create')}}" class="btn btn-primary btn-xs">
                                <i class="fa fa-plus"></i> Inserir
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Produto</th>
                            <th scope="col">Fornecedor</th>
                            <th scope="col">Tamanho</th>
                            <th scope="col">Quantidade</th>
                            <th scope="col" >Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($estoques as $estoque)
                            <tr>
                                <td scope="row">{{$estoque->id}}</td>
                                <td scope="row">{{$estoque->produto->nome}}</td>
                                <td scope="row">{{$estoque->fornecedor->nome  or ''}}</td>
                                <td scope="row">{{$estoque->tamanhoProduto->nome or ''}}</td>
                                <td scope="row">{{$estoque->quantidade}}</td>
                                <td scope="row">
                                    @component('components.actions', ['entidade' => 'estoque', 'id' => $estoque->id])@endcomponent
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

@endsection