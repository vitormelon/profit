<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('unidade', 'Unidade')}}
            {{Form::select('unidade_id', $unidades, null, ['class' => 'form-control', 'select2' => 'true'])}}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('produto', 'Produto')}}
            {{Form::select('produto_id', $produtos, null, ['class' => 'form-control' , 'placeholder' => 'Selecione...', 'select2' => 'true'])}}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('fornecedor', 'Fornecedor')}}
            {{Form::select('fornecedor_id', $fornecedores, null, ['class' => 'form-control' , 'placeholder' => 'Selecione...', 'select2' => 'true'])}}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('tamanho', 'Tamanho')}}
            {{Form::select('tamanho_produto_id', $tamanhosProduto, null, ['class' => 'form-control' , 'placeholder' => 'Selecione...', 'select2' => 'true'])}}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('quantidade', 'Quantidade')}}
            {{Form::text('quantidade', null, array('placeholder' => '','class' => 'form-control'))}}

        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {{Form::label('valorCusto', 'Custo')}}
            {{Form::text('valorCusto', null, array('placeholder' => '','class' => 'form-control'))}}

        </div>
    </div>
</div>
<button type="submit" class="btn btn-primary">Gravar</button>