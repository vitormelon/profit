function buttonConfirmation(e){
    var msg = $(e).attr('data-confirm'); // Get the confirm message
    return confirm(msg);
}
function updateSelect2(){
    $('select[select2=true]').select2();

    $('select[select2Tag=true]').select2({
        tags:true
    });
}
$(document).ready(function() {
    updateSelect2();
});

