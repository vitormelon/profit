<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class unidade extends Model
{
    protected $fillable = ['nome', 'ativo'];

    public static function getAllActives(){
        return unidade::where('ativo', 1)->get();
    }

    public function getSelectFormatAttribute()
    {
        return $this->id . " - " . $this->nome;
    }

    public static function getAllForSelect(){
        return unidade::getAllActives()->pluck('select_format', 'id');
    }
}
