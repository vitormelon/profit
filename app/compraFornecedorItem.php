<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class compraFornecedorItem extends Model
{
    protected $fillable = ['quantidade', 'valorCusto', 'compra_fornecedor_id', 'tamanho_produto_id', 'produto_id'];

    protected $table = 'compra_fornecedor_itens';

    public function compraFornecedor(){
        return $this->belongsTo(compraFornecedor::class);
    }

    public function tamanhoProduto(){
        return $this->belongsTo(tamanhoProduto::class);
    }

    public function produto(){
        return $this->belongsTo(produto::class);
    }
}
