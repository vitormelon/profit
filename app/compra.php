<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class compra extends Model
{
    protected $fillable = ['unidade_id', 'descricao', 'data', 'observacoes'];

    protected $dates = ['data'];

    public function compraFornecedores(){
        return $this->hasMany(compraFornecedor::class);
    }

    public function fornecedores()
    {
        return $this->belongsToMany('App\fornecedor', 'compra_fornecedores');
    }

    public function unidade(){
        return $this->belongsTo(unidade::class);
    }

}
