<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tamanhoProduto extends Model
{
    protected $table = 'tamanhos_produto';
    protected $fillable = ['descricao', 'nome', 'ativo'];

    /**
     * @param $value
     */
    public function setSiglaAttribute($value)
    {
        $this->attributes['nome'] = strtoupper($value);
    }

    public static function getAllActives(){
        return tamanhoProduto::where('ativo', 1)->get();
    }

    public function getSelectFormatAttribute()
    {
        return $this->id . " - " . $this->nome;
    }

    public static function getAllForSelect(){
        return tamanhoProduto::getAllActives()->pluck('select_format', 'id');
    }
}
