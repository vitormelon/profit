<?php

namespace App\Repositories;


use App\compra;
use App\compraFornecedor;
use App\fornecedor;
use App\Repositories\Interfaces\ICompraFornecedorRepository;
use Illuminate\Http\Request;

class CompraFornecedorRepository implements ICompraFornecedorRepository
{
    /**
     * @param string $fornecedor
     * @param compra $compra
     * @return compraFornecedor
     */
    public function add(string $fornecedor, compra $compra):compraFornecedor
    {
        if(is_numeric($fornecedor)){
            return $this->createCompraFornecedor(intval($fornecedor), $compra);
        }else{
            $createdFornecedor = fornecedor::addFornecedorBy($fornecedor);
            return $this->createCompraFornecedor($createdFornecedor->id, $compra);
        }
    }

    private function createCompraFornecedor(int $fornecedor, compra $compra):compraFornecedor
    {
        $compraFornecedor = new compraFornecedor();
        $compraFornecedor->fornecedor()->associate($fornecedor);
        $compraFornecedor->compra()->associate($compra);
        if($compraFornecedor->save()){
            return $compraFornecedor;
        }
        return null;
    }
}