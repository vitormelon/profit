<?php


namespace App\Repositories\Interfaces;


use App\compraFornecedorItem;
use Illuminate\Http\Request;

interface ICompraFornecedorItemRepository
{
    public function add(request $request):compraFornecedorItem;
}