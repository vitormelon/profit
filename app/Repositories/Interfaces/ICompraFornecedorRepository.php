<?php


namespace App\Repositories\Interfaces;


use App\compra;
use App\compraFornecedor;
use Illuminate\Http\Request;

interface ICompraFornecedorRepository
{
    public function add(string $fornecedor, compra $compra):compraFornecedor;
}