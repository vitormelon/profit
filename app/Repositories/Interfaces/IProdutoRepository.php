<?php


namespace App\Repositories\Interfaces;


use App\produto;
use Illuminate\Http\Request;

interface IProdutoRepository
{
    public function deleteImageAndThumb(Produto $produto);
    public function addProduto(Request $request): produto;
    public function updateProduto(Request $request, String $id): bool;
}