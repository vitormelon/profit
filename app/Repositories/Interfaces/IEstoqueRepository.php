<?php


namespace App\Repositories\Interfaces;


use App\estoque;
use Illuminate\Http\Request;

interface IEstoqueRepository
{
    public function add(Request $request):estoque;
    public function update(Request $request, string $id):bool;
}