<?php

namespace App\Repositories;

use App\compraFornecedorItem;
use App\Repositories\Interfaces\ICompraFornecedorItemRepository;
use App\Repositories\Interfaces\IProdutoRepository;
use Illuminate\Http\Request;

class CompraFornecedorItemRepository implements ICompraFornecedorItemRepository
{
    protected $iProdutoRepository;
    public function __construct(IProdutoRepository $iProdutoRepository)
    {
        $this->iProdutoRepository = $iProdutoRepository;
    }

    /**
     * @param Request $request
     * @return estoque
     */
    public function add(request $request):compraFornecedorItem
    {
        $produto = $this->iProdutoRepository->addProduto($request);
        $request->request->add(['produto_id' => $produto->id]);

        $compraFornecedorItem = compraFornecedorItem::create($request->all());
        return $compraFornecedorItem;
    }

}