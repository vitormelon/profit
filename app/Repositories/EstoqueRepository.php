<?php

namespace App\Repositories;


use App\estoque;
use App\Repositories\Interfaces\IEstoqueRepository;
use Illuminate\Http\Request;

class EstoqueRepository implements IEstoqueRepository
{
    /**
     * @param Request $request
     * @return estoque
     */
    public function add(Request $request):estoque
    {
        $request->validate([
            //'nome' => 'required|unique:estoques'
        ]);
        return estoque::create($request->all());
    }


    /**
     * @param Request $request
     * @param string $id
     * @return bool
     */
    public function update(Request $request, string $id):bool
    {
        $estoque = estoque::find($id);
        $request->validate([
            //'nome' => ['required', Rule::unique('estoques')->ignore($id)]
        ]);
        $input = $request->all();
        return $estoque->update($input);
    }
}