<?php

namespace App\Repositories;


use App\produto;
use App\Repositories\Interfaces\IProdutoRepository;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class ProdutoRepository implements IProdutoRepository
{
    /**
     * @param produto $produto
     */
    public function deleteImageAndThumb(Produto $produto){
        $filePath = public_path($produto->path . $produto->imagem);
        $thumbFilePath = public_path($produto->path . "thumb". $produto->imagem);
        $this->deleteFileIfExists($filePath);
        $this->deleteFileIfExists($thumbFilePath);
    }

    /**
     * @param Request $request
     * @return produto
     */
    public function addProduto(Request $request): produto
    {
        $imageName = $this->storeImage($request);

        $input = $request->all();
        $input['imagem'] = $imageName;
        return Produto::create($input);
    }

    /**
     * @param Request $request
     * @param String $id
     * @return bool
     */
    public function updateProduto(Request $request, String $id): bool
    {
        $produto = Produto::find($id);
        $this->deleteImageAndThumb($produto);

        $imageName = $this->storeImage($request);

        $input = $request->all();
        $input['imagem'] = $imageName;
        return $produto->update($input);
    }

    /**
     * Store image
     *
     * @param Request $request
     * @return String
     */
    private function storeImage(Request $request){
        if($request->imagem == null){
            return;
        }

        $image       = $request->imagem;
        $filename    = time().'.'.$request->imagem->getClientOriginalExtension();

        $this->resizeAndSaveImage($image, $filename, 1000, 1000);
        $this->resizeAndSaveImage($image, "thumb".$filename, 300, 300);

        return $filename;
    }

    /**
     * @param $image
     * @param $filename
     * @param int $maxWidth
     * @param int $maxHeight
     */
    private function resizeAndSaveImage($image, $filename, int $maxWidth, int $maxHeight)
    {
        $image_resize = Image::make($image->getRealPath());

        $image_resize->height() > $image_resize->width() ? $maxWidth = null : $maxHeight = null;
        $image_resize->resize($maxWidth, $maxHeight, function ($constraint) {
            $constraint->aspectRatio();
        });

        $image_resize->save(public_path(Produto::$imagePath . $filename));
    }


    /**
     * @param $filePath
     */
    private function deleteFileIfExists($filePath)
    {
        if (file_exists($filePath) && is_file($filePath)) {
            unlink($filePath);
        }
    }

}