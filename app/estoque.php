<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estoque extends Model
{
    protected $fillable = ['quantidade', 'valorCusto', 'unidade_id', 'fornecedor_id', 'tamanho_produto_id', 'produto_id'];

    public function unidade(){
        return $this->belongsTo(unidade::class);
    }

    public function fornecedor(){
        return $this->belongsTo(fornecedor::class);
    }

    public function tamanhoProduto(){
        return $this->belongsTo(tamanhoProduto::class);
    }

    public function produto(){
        return $this->belongsTo(produto::class);
    }
    public function compras(){
        return $this->hasMany(compra::class);
    }
}
