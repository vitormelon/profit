<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $fillable = ['nome','endereco','instagram','facebook','email','celular','telefone','observacoes'];

    public static function getAllActives(){
        return Clientes::where('ativo', 1)->get();
    }

    public function getSelectFormatAttribute()
    {
        return $this->id . " - " . $this->nome;
    }

    public static function getAllForSelect(){
        return Clientes::getAllActives()->pluck('select_format', 'id');
    }
}
