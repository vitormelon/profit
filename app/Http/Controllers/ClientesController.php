<?php

namespace App\Http\Controllers;

use App\Clientes;
use Illuminate\Http\Request;

class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Clientes::all();
        return view('clientes.index', compact('clientes', $clientes));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("clientes.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nome' => 'required|min:3'
        ]);

        $cliente = Clientes::create($request->all());
        return redirect('clientes');
    }

    /**
     * Display the specified resource.Rua
     *
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function show(Clientes $cliente)
    {
        return view('clientes.show', compact('cliente', $cliente));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function edit(Clientes $cliente)
    {
        return view('clientes.edit', compact('cliente', $cliente));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  String $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, String $id)
    {
        $cliente = Clientes::find($id);
        $request->validate([
            'nome' => 'required|min:3'
        ]);
        $input = $request->all();
        $cliente->update($input);

        $request->session()->flash("message", "Cliente modificado com sucesso");


        return redirect('clientes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param String $id
     * @return void
     */
    public function destroy(String $id)
    {
        $cliente = Clientes::findOrFail($id);
        $cliente->delete();
        return redirect('clientes')->with("message", "Cliente excluido com sucesso");
    }
}
