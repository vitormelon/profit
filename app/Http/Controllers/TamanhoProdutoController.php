<?php

namespace App\Http\Controllers;

use App\tamanhoProduto;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TamanhoProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tamanhosProduto = tamanhoProduto::all();
        return view('tamanhoProduto.index', compact('tamanhosProduto', $tamanhosProduto));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("tamanhoProduto.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nome' => 'required|max:3|unique:tamanhos_produto'
        ]);

        $tamanhoProduto = tamanhoProduto::create($request->all());
        return redirect('tamanhoProduto');
    }

    /**
     * Display the specified resource.Rua
     *
     * @param  \App\tamanhoProduto  $tamanhoProduto
     * @return \Illuminate\Http\Response
     */
    public function show(tamanhoProduto $tamanhoProduto)
    {
        return view('tamanhoProduto.show', compact('tamanhoProduto', $tamanhoProduto));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tamanhoProduto  $tamanhoProduto
     * @return \Illuminate\Http\Response
     */
    public function edit(tamanhoProduto $tamanhoProduto)
    {
        return view('tamanhoProduto.edit', compact('tamanhoProduto', $tamanhoProduto));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  String $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, String $id)
    {
        $tamanhoProduto = tamanhoProduto::find($id);
        $request->validate([
            'nome' => ['required', 'max:3', Rule::unique('tamanhos_produto')->ignore($id)]
        ]);
        $input = $request->all();
        $tamanhoProduto->update($input);

        $request->session()->flash("message", "Tamanho Produto modificado com sucesso");


        return redirect('tamanhoProduto');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param String $id
     * @return void
     */
    public function destroy(String $id)
    {
        $tamanhoProduto = tamanhoProduto::findOrFail($id);
        $tamanhoProduto->delete();
        return redirect('tamanhoProduto')->with("message", "Tamanho Produto excluido com sucesso");
    }
}
