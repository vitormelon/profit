<?php

namespace App\Http\Controllers;

use App\produto;
use App\Repositories\Interfaces\IProdutoRepository;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    protected $iProdutoRepository;

    public function __construct(IProdutoRepository $iProdutoRepository)
    {
        $this->iProdutoRepository = $iProdutoRepository;
    }
    public function index()
    {
        $produtos = Produto::all();
        return view('produto.index', compact('produtos', $produtos));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("produto.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->iProdutoRepository->addProduto($request);
        return redirect('produto');
    }

    /**
     * Display the specified resource.Rua
     *
     * @param  \App\Produto  $produtos
     * @return \Illuminate\Http\Response
     */
    public function show(Produto $produto)
    {
        return view('produtos.show', compact('produto', $produto));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Produto  $produtos
     * @return \Illuminate\Http\Response
     */
    public function edit(Produto $produto)
    {
        return view('produto.edit', compact('produto', $produto));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  String $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, String $id)
    {
        $this->iProdutoRepository->updateProduto($request, $id);

        $request->session()->flash("message", "Produto modificado com sucesso");

        return redirect('produto');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param String $id
     * @return
     */
    public function destroy(String $id)
    {
        $produto = Produto::findOrFail($id);
        $this->iProdutoRepository->deleteImageAndThumb($produto);
        $produto->delete();
        return redirect('produto')->with("message", "Produto excluido com sucesso");
    }

    public function getNextID(){
        return produto::getNextID();
    }

}
