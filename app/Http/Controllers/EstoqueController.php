<?php

namespace App\Http\Controllers;

use App\estoque;
use App\fornecedor;
use App\produto;
use App\Repositories\Interfaces\IEstoqueRepository;
use App\tamanhoProduto;
use App\unidade;
use Illuminate\Http\Request;

class EstoqueController extends Controller
{

    protected $iEstoqueRepository;

    public function __construct(IEstoqueRepository $iEstoqueRepository)
    {
        $this->iEstoqueRepository = $iEstoqueRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estoques = estoque::all();

        return view('estoque.index', compact('estoques', $estoques));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $produtos = produto::getAllForSelect();
        $fornecedores = fornecedor::getAllForSelect();
        $tamanhosProduto = tamanhoProduto::getAllForSelect();
        $unidades = unidade::getAllForSelect();
        return view("estoque.create")
                ->with(compact('produtos', $produtos))
                ->with(compact('fornecedores', $fornecedores))
                ->with(compact('unidades', $unidades))
                ->with(compact('tamanhosProduto', $tamanhosProduto));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->iEstoqueRepository->add($request);
        return redirect('estoque');
    }

    /**
     * Display the specified resource.Rua
     *
     * @param  \App\estoque  $estoque
     * @return \Illuminate\Http\Response
     */
    public function show(estoque $estoque)
    {
        return view('estoque.show', compact('estoque', $estoque));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\estoque  $estoque
     * @return \Illuminate\Http\Response
     */
    public function edit(estoque $estoque)
    {
        $produtos = produto::getAllForSelect();
        $fornecedores = fornecedor::getAllForSelect();
        $tamanhosProduto = tamanhoProduto::getAllForSelect();
        $unidades = unidade::getAllForSelect();

        return view('estoque.edit', compact('estoque', $estoque))
            ->with(compact('produtos', $produtos))
            ->with(compact('fornecedores', $fornecedores))
            ->with(compact('unidades', $unidades))
            ->with(compact('tamanhosProduto', $tamanhosProduto));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  String $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, String $id)
    {
        $this->iEstoqueRepository->update($request, $id);

        $request->session()->flash("message", "Estoque modificado com sucesso");


        return redirect('estoque');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param String $id
     * @return void
     */
    public function destroy(String $id)
    {
        $estoque = estoque::findOrFail($id);
        $estoque->delete();
        return redirect('estoque')->with("message", "Estoque excluido com sucesso");
    }
}
