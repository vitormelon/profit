<?php

namespace App\Http\Controllers;

use App\fornecedor;
use Illuminate\Http\Request;

class FornecedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fornecedores = fornecedor::all();
        return view('fornecedor.index', compact('fornecedores', $fornecedores));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("fornecedor.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nome' => 'required'
        ]);

        $fornecedor = fornecedor::create($request->all());
        return redirect('fornecedor');
    }

    /**
     * Display the specified resource.Rua
     *
     * @param  \App\fornecedor  $fornecedor
     * @return \Illuminate\Http\Response
     */
    public function show(fornecedor $fornecedor)
    {
        return view('fornecedor.show', compact('fornecedor', $fornecedor));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\fornecedor  $fornecedor
     * @return \Illuminate\Http\Response
     */
    public function edit(fornecedor $fornecedor)
    {
        return view('fornecedor.edit', compact('fornecedor', $fornecedor));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  String $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, String $id)
    {
        $fornecedor = fornecedor::find($id);
        $request->validate([
            'nome' => 'required'
        ]);
        $input = $request->all();
        $fornecedor->update($input);

        $request->session()->flash("message", "Fornecedor modificado com sucesso");


        return redirect('fornecedor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param String $id
     * @return void
     */
    public function destroy(String $id)
    {
        try{
            $fornecedor = fornecedor::findOrFail($id);
            $fornecedor->delete();
            return redirect('fornecedor')->with("message", "Fornecedor excluido com sucesso");
        }catch (\Exception $e){
            return redirect('fornecedor')->with("message", "Não foi possível excluir o Fornecedor");
        }

    }
}
