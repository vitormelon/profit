<?php

namespace App\Http\Controllers;

use App\fornecedor;
use App\produto;
use App\tamanhoProduto;
use App\unidade;
use App\compra;
use Illuminate\Http\Request;

class CompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $compras = compra::all();

        return view('compra.index', compact('compras', $compras));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fornecedores = fornecedor::getAllForSelect();
        $unidades = unidade::getAllForSelect();
        $produtos = produto::getAllForSelect();

        return view('compra.create')
            ->with(compact('fornecedores'))
            ->with(compact('unidades'))
            ->with(compact('produtos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            //'nome' => 'required|unique:compras'
        ]);

        $input = $request->all();

        $compra = compra::create($input);
        return redirect()->route('compra.edit', $compra->id);
    }

    /**
     * Display the specified resource.Rua
     *
     * @param  \App\compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function show(compra $compra)
    {
        return view('compra.show', compact('compra', $compra));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function edit(compra $compra)
    {
        $unidades = unidade::getAllForSelect();
        $produtos = produto::getAllForSelect();
        $tamanhosProduto = tamanhoProduto::getAllForSelect();

        return view('compra.edit', compact('compra', $compra))
            ->with(compact('tamanhosProduto'))
            ->with(compact('unidades'))
            ->with(compact('produtos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  String $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, String $id)
    {
        $compra = compra::find($id);
        $request->validate([
            //'nome' => ['required', Rule::unique('compras')->ignore($id)]
        ]);
        $input = $request->all();
        $compra->update($input);

        $request->session()->flash("message", "Compra modificado com sucesso");


        return redirect()->route('compra.edit', $compra->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param String $id
     * @return void
     */
    public function destroy(String $id)
    {
        $compra = compra::findOrFail($id);
        $compra->delete();
        return redirect('compra')->with("message", "Compra excluido com sucesso");
    }
}
