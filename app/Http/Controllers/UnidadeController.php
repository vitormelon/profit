<?php

namespace App\Http\Controllers;

use App\unidade;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UnidadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unidades = unidade::all();
        return view('unidade.index', compact('unidades', $unidades));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("unidade.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nome' => 'required|unique:unidades'
        ]);

        $unidade = unidade::create($request->all());
        return redirect('unidade');
    }

    /**
     * Display the specified resource.Rua
     *
     * @param  \App\unidade  $unidade
     * @return \Illuminate\Http\Response
     */
    public function show(unidade $unidade)
    {
        return view('unidade.show', compact('unidade', $unidade));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\unidade  $unidade
     * @return \Illuminate\Http\Response
     */
    public function edit(unidade $unidade)
    {
        return view('unidade.edit', compact('unidade', $unidade));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  String $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, String $id)
    {
        $unidade = unidade::find($id);
        $request->validate([
            'nome' => ['required', Rule::unique('unidades')->ignore($id)]
        ]);
        $input = $request->all();
        $unidade->update($input);

        $request->session()->flash("message", "Unidade modificada com sucesso");


        return redirect('unidade');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param String $id
     * @return void
     */
    public function destroy(String $id)
    {
        $unidade = unidade::findOrFail($id);
        $unidade->delete();
        return redirect('unidade')->with("message", "Unidade excluida com sucesso");
    }
}
