<?php

namespace App\Http\Controllers;

use App\compra;
use App\compraFornecedor;
use App\fornecedor;
use App\produto;
use App\Repositories\Interfaces\ICompraFornecedorItemRepository;
use App\Repositories\Interfaces\ICompraFornecedorRepository;
use App\Repositories\Interfaces\IEstoqueRepository;
use Illuminate\Http\Request;

class CompraRestController extends Controller
{

    protected $iEstoqueRepository;
    protected $iCompraFornecedorRepository;
    protected $iCompraFornecedorItemRepository;

    public function __construct(IEstoqueRepository $iEstoqueRepository, ICompraFornecedorRepository $iCompraFornecedorRepository, ICompraFornecedorItemRepository $iCompraFornecedorItemRepository)
    {
        $this->iEstoqueRepository = $iEstoqueRepository;
        $this->iCompraFornecedorRepository = $iCompraFornecedorRepository;
        $this->iCompraFornecedorItemRepository = $iCompraFornecedorItemRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $compra = compra::find($id);
        $compraFornecedores = compraFornecedor::with('fornecedor', 'compraFornecedorItem', 'produtos')->where('compra_id', $id)->get();
        $fornecedores = fornecedor::select('id', 'nome')->whereNotIn('id', $compraFornecedores->pluck('fornecedor_id'))->where('ativo', 1)->get();
        $produtos = produto::select('id', 'nome')->get();

        return response()->json([   'compra' => $compra,
                                    'fornecedores' => $fornecedores,
                                    'produtos' => $produtos,
                                    'compraFornecedores' => $compraFornecedores]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  String $id
     * @return \Illuminate\Http\Response
     * @return int|string
     * @throws \Exception
     */
    public function update(Request $request, String $id)
    {
        $compra = compra::find($id);
        if(isset($request->addfornecedor)){
            return $this->iCompraFornecedorRepository->add($request->addfornecedor, $compra);
        }elseif(isset($request->removefornecedor)){
            return $this->removeFornecedor($request->removefornecedor, $compra);
        }elseif(isset($request->addproduto)){
            return $this->iCompraFornecedorItemRepository->add($request);
            //return $this->removeProduto($request->removeproduo, $compra);
        }

        throw new \Exception("Erro");
    }

    private function removeFornecedor(string $id, compra $compra){
        $compra->fornecedores()->detach($id);
        return $id;
    }


}
