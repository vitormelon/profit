<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class produto extends Model
{
    public static $imagePath = 'arquivos/produtos/';
    public $path = 'arquivos/produtos/';
    protected $fillable = ['nome','imagem','codigo','precoVenda','precoCusto','descricao'];

    public function estoque(){
        return $this->hasMany(estoque::class);
    }

    public function getSelectFormatAttribute()
    {
        return $this->codigo . " - " . $this->nome;
    }

    public function getQuantidadeEstoqueAttribute(){
        $somatorio = 0;
        foreach ($this->estoque as $estoque) {
            $somatorio += $estoque->quantidade;
        }
        return $somatorio;
    }

    public static function getAllActives(){
        return produto::where('ativo', 1)->get();
    }

    public static function getAllForSelect(){
        return produto::getAllActives()->pluck('select_format', 'id');
    }

    public static function getNextID()
    {
        $statement = DB::select("show table status like 'produtos'");
        return response()->json(['id' => $statement[0]->Auto_increment]);
    }
}
