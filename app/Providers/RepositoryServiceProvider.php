<?php

namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $models = [
            'Estoque',
            'Produto',
            'CompraFornecedor',
            'CompraFornecedorItem'
        ];

        foreach ($models as $model) {
            $this->app->bind(
                "App\Repositories\Interfaces\\I{$model}Repository",
                "App\Repositories\\{$model}Repository"
            );
        }
    }
}