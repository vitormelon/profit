<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fornecedor extends Model
{
    protected $table = 'fornecedores';

    protected $fillable = ['nome','endereco','cpfcnpj','telefone1','telefone2','telefone3', 'observacoes', 'ativo'];

    public static function getAllActives(){
        return fornecedor::where('ativo', 1)->get();
    }

    public function getSelectFormatAttribute()
    {
        return $this->id . " - " . $this->nome;
    }

    public static function getAllForSelect(){
        return fornecedor::getAllActives()->pluck('select_format', 'id');
    }

    public static function addFornecedorBy($nome):fornecedor{
        return fornecedor::create(['nome' => $nome]);
    }
}