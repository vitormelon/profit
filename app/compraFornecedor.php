<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class compraFornecedor extends Model
{
    protected $fillable = ['fornecedor_id', 'compra_id'];

    protected $table = 'compra_fornecedores';

    public function fornecedor(){
        return $this->belongsTo(fornecedor::class);
    }

    public function produtos()
    {
        return $this->belongsToMany('App\produto', 'compra_fornecedor_itens')->withPivot('quantidade', 'valorCusto', 'tamanho_produto_id');
    }

    public function compra(){
        return $this->belongsTo(compra::class);
    }

    public function compraFornecedorItem(){
        return $this->hasMany(compraFornecedorItem::class);
    }
}
